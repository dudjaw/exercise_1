package bmiinterface;

/**
 * 
 * @author János Vaszca
 *
 */
public class Calculator implements BMICalculator {

	public void calculateBMI(Person p) throws InvalidInput {

		if (!((p.getWeighttype().equals("kg")) || (p.getWeighttype().equals("dkg")) || (p.getWeighttype().equals("g"))
				|| (p.getWeighttype().equals("pound")))) {
			throw new InvalidInput("Invalid weight type.");
		}
		if (!(p.getHeighttype().equals("m") || p.getHeighttype().equals("cm") || p.getHeighttype().equals("mm")
				|| p.getHeighttype().equals("inch"))) {
			throw new InvalidInput("Invalid height type");
		}
		try {
			if (p.getHeighttype().equals("m") && p.getWeighttype().equals("kg")) {
				calcKgM(p);
			}

			if (p.getHeighttype().equals("m") && p.getWeighttype().equals("dkg")) {
				calcDkgM(p);
			}

			if (p.getHeighttype().equals("m") && p.getWeighttype().equals("g")) {
				calcGM(p);
			}

			if (p.getHeighttype().equals("m") && p.getWeighttype().equals("pound")) {
				calcPM(p);
			}

			if (p.getHeighttype().equals("cm") && p.getWeighttype().equals("kg")) {
				calcKgCm(p);
			}

			if (p.getHeighttype().equals("cm") && p.getWeighttype().equals("dkg")) {
				calcDkgCm(p);
			}

			if (p.getHeighttype().equals("cm") && p.getWeighttype().equals("g")) {
				calcGCm(p);
			}

			if (p.getHeighttype().equals("cm") && p.getWeighttype().equals("pound")) {
				calcPCm(p);
			}

			if (p.getHeighttype().equals("mm") && p.getWeighttype().equals("kg")) {
				calcKgMm(p);
			}

			if (p.getHeighttype().equals("mm") && p.getWeighttype().equals("dkg")) {
				calcDkgMm(p);
			}

			if (p.getHeighttype().equals("mm") && p.getWeighttype().equals("g")) {
				calcGMm(p);
			}

			if (p.getHeighttype().equals("mm") && p.getWeighttype().equals("pound")) {
				calcPMm(p);
			}

			if (p.getHeighttype().equals("inch") && p.getWeighttype().equals("kg")) {
				calcKgInch(p);
			}

			if (p.getHeighttype().equals("inch") && p.getWeighttype().equals("dkg")) {
				calcDkgInch(p);
			}

			if (p.getHeighttype().equals("inch") && p.getWeighttype().equals("g")) {
				calcGInch(p);
			}

			if (p.getHeighttype().equals("inch") && p.getWeighttype().equals("pound")) {
				calcPInch(p);
			}

			addVerdict(p);

		} catch (InvalidInput ex) {
			throw new InvalidInput(ex.getMessage());
		}
	}

	private void calcKgM(Person per) throws InvalidInput {
		if (per.getHeight() < 0.5 || per.getHeight() > 2.5 || per.getWeight() > 300 || per.getWeight() < 10) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight(), per.getHeight()));
	}

	private void calcKgCm(Person per) throws InvalidInput {
		if (per.getHeight() < 50 || per.getHeight() > 250 || per.getWeight() > 300 || per.getWeight() < 10) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight(), per.getHeight() / 100));
	}

	private void calcKgMm(Person per) throws InvalidInput {
		if (per.getHeight() < 500 || per.getHeight() > 2500 || per.getWeight() > 300 || per.getWeight() < 10) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight(), per.getHeight() / 1000));
	}

	private void calcKgInch(Person per) throws InvalidInput {
		if (per.getHeight() < 20 || per.getHeight() > 99 || per.getWeight() > 300 || per.getWeight() < 10) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight(), per.getHeight() * 0.0254));
	}

	private void calcDkgM(Person per) throws InvalidInput {
		if (per.getHeight() < 0.5 || per.getHeight() > 2.5 || per.getWeight() > 30000 || per.getWeight() < 1000) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight() / 100, per.getHeight()));
	}

	private void calcDkgCm(Person per) throws InvalidInput {
		if (per.getHeight() < 50 || per.getHeight() > 250 || per.getWeight() > 30000 || per.getWeight() < 1000) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight() / 100, per.getHeight() / 100));
	}

	private void calcDkgMm(Person per) throws InvalidInput {
		if (per.getHeight() < 500 || per.getHeight() > 2500 || per.getWeight() > 30000 || per.getWeight() < 1000) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight() / 100, per.getHeight() / 1000));
	}

	private void calcDkgInch(Person per) throws InvalidInput {
		if (per.getHeight() < 20 || per.getHeight() > 99 || per.getWeight() > 30000 || per.getWeight() < 1000) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight() / 100, per.getHeight() * 0.0254));
	}

	private void calcGM(Person per) throws InvalidInput {
		if (per.getHeight() < 0.5 || per.getHeight() > 2.5 || per.getWeight() > 300000 || per.getWeight() < 10000) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight() / 1000, per.getHeight()));
	}

	private void calcGCm(Person per) throws InvalidInput {
		if (per.getHeight() < 50 || per.getHeight() > 250 || per.getWeight() > 300000 || per.getWeight() < 10000) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight() / 1000, per.getHeight() / 100));
	}

	private void calcGMm(Person per) throws InvalidInput {
		if (per.getHeight() < 500 || per.getHeight() > 2500 || per.getWeight() > 300000 || per.getWeight() < 10000) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight() / 1000, per.getHeight() / 1000));
	}

	private void calcGInch(Person per) throws InvalidInput {
		if (per.getHeight() < 20 || per.getHeight() > 99 || per.getWeight() > 300000 || per.getWeight() < 10000) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight() / 1000, per.getHeight() * 0.0254));
	}

	private void calcPInch(Person per) throws InvalidInput {
		if (per.getHeight() < 20 || per.getHeight() > 99 || per.getWeight() > 662 || per.getWeight() < 22) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaPINCH(per.getWeight(), per.getHeight()));
	}

	private void calcPM(Person per) throws InvalidInput {
		if (per.getHeight() < 0.5 || per.getHeight() > 2.5 || per.getWeight() > 662 || per.getWeight() < 22) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight() * 0.4536, per.getHeight()));
	}

	private void calcPCm(Person per) throws InvalidInput {
		if (per.getHeight() < 50 || per.getHeight() > 250 || per.getWeight() > 662 || per.getWeight() < 22) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight() * 0.4536, per.getHeight() / 100));
	}

	private void calcPMm(Person per) throws InvalidInput {
		if (per.getHeight() < 500 || per.getHeight() > 2500 || per.getWeight() > 662 || per.getWeight() < 22) {
			throw new InvalidInput("Invalid Input!");
		}
		per.setBMI(bmiFormulaKGM(per.getWeight() * 0.4536, per.getHeight() / 1000));
	}

	private void addVerdict(Person per) {
		if (per.getBMI() < 18.5) {
			per.setVerdict("Underweight!");
		}

		if (per.getBMI() >= 18.5 && per.getBMI() < 25) {
			per.setVerdict("Normal weight!");
		}

		if (per.getBMI() >= 25 && per.getBMI() < 30) {
			per.setVerdict("Overweight!");
		}

		if (per.getBMI() > 30) {
			per.setVerdict("Obese!");
		}
	}

	private double bmiFormulaKGM(double weight, double height) {
		return (weight / (height * height));
	}

	private double bmiFormulaPINCH(double weight, double height) {
		return (weight * 703) / (height * height);
	}

}
