package bmiinterface;

public class Person {

	private String name;
	private String weighttype;
	private double weight;
	private String heighttype;
	private double height;
	private double BMI;
	private String verdict;

	public Person() {
		super();
		this.name = "NULL";
		this.weighttype = "NULL";
		this.weight = 0;
		this.heighttype = "NULL";
		this.height = 0;
		this.BMI = 0.0;
		this.verdict = "NULL";
	}

	/**
	 * 
	 * @param name
	 *            Name of the person
	 * @param weight
	 *            Weight of the person
	 * @param weighttype
	 *            [kg, dkg, g, pound]
	 * @param height
	 *            Weight of the person
	 * @param heighttype
	 *            [m, cm, mm, inch]
	 */
	public Person(String name, double weight, String weighttype, double height, String heighttype) {
		super();
		this.name = name;
		this.weighttype = weighttype;
		this.weight = weight;
		this.heighttype = heighttype;
		this.height = height;
	}

	public double getBMI() {
		return BMI;
	}

	public String getVerdict() {
		return verdict;
	}

	protected void setBMI(double bMI) {
		BMI = bMI;
	}

	protected void setVerdict(String verdict) {
		this.verdict = verdict;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWeighttype() {
		return weighttype;
	}

	public void setWeighttype(String weighttype) {
		this.weighttype = weighttype;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getHeighttype() {
		return heighttype;
	}

	public void setHeighttype(String heighttype) {
		this.heighttype = heighttype;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

}
