package bmiinterface;


public interface BMICalculator {
	/**
	 * 
	 * @param p a Person object
	 * @throws InvalidInput if the types and miss-match the ones specified in Person class constructor,
	 * or if the numbers are not in the valid interval.
	 * Intervals: 
	 * m: 0.5-2.5, 
	 * cm: 50-250, 
	 * mm: 500-2500, 
	 * inch: 20-99, 
	 * kg: 10-300, 
	 * dkg: 1000-3000, 
	 * g: 10000-30000, 
	 * pound: 22-662, 
	 */
	public void calculateBMI(Person p)  throws InvalidInput;
}
