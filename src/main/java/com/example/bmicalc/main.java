package com.example.bmicalc;

import bmiinterface.BMICalculator;
import bmiinterface.Calculator;
import bmiinterface.InvalidInput;
import bmiinterface.Person;

public class main {

	public static void demoValid(Person p, BMICalculator a) {
		try {
			a.calculateBMI(p);
			System.out.println(p.getName() + " BMI: " + p.getBMI() + " " + p.getVerdict());
		} catch (InvalidInput ex) {
			System.out.println(ex.getMessage());
		}
	}

	public static void demoInvalid(Person p, BMICalculator a) {
		try {
			a.calculateBMI(p);
			System.out.println(p.getName() + " BMI: " + p.getBMI() + " " + p.getVerdict());
		} catch (InvalidInput ex) {
			System.out.println(ex.getMessage());
		}
	}

	public static void main(String[] args) {
		BMICalculator calc = new Calculator();

		Person pervalone = new Person("First Valid", 90, "kg", 2, "m");
		Person pervaltwo = new Person("Second Valid", 200, "pound", 70, "inch");
		Person pervalthird = new Person("Thrid Valid", 7000, "dkg", 2, "m");
		Person perinvalone = new Person("First Invalid", 100, "aa", 100, "aa");
		Person perinvaltwo = new Person("Second Invalid", 500, "kg", 100, "cm");
		Person perinvalthree = new Person("Third Invalid", 700, "pound", 80, "inch");
		demoValid(pervalone, calc);
		demoInvalid(perinvalone, calc);
		demoValid(pervaltwo, calc);
		demoValid(pervalthird, calc);
		demoInvalid(perinvaltwo, calc);
		demoInvalid(perinvalthree, calc);

	}

}
