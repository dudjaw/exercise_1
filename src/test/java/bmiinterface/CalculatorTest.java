package bmiinterface;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
	Person perinv = new Person();

	Person perkgm = new Person();
	Person perkgminv = new Person();

	Person perdkgm = new Person();
	Person perdkgminv = new Person();

	Person pergm = new Person();
	Person pergminv = new Person();

	Person perpm = new Person();
	Person perpminv = new Person();

	Person perkgcm = new Person();
	Person perkgcminv = new Person();

	Person perdkgcm = new Person();
	Person perdkgcminv = new Person();

	Person pergcm = new Person();
	Person pergcminv = new Person();

	Person perpcm = new Person();
	Person perpcminv = new Person();

	Person perkgmm = new Person();
	Person perkgmminv = new Person();

	Person perdkgmm = new Person();
	Person perdkgmminv = new Person();

	Person pergmm = new Person();
	Person pergmminv = new Person();

	Person perpmm = new Person();
	Person perpmminv = new Person();

	Person perkgi = new Person();
	Person perkgiinv = new Person();

	Person perdkgi = new Person();
	Person perdkgiinv = new Person();

	Person pergi = new Person();
	Person pergiinv = new Person();

	Person perpi = new Person();
	Person perpiinv = new Person();

	BMICalculator calc = new Calculator();

	@Before
	public void setUp() {
		perinv = new Person("Name", 0, "lk", 0, "al");

		perkgm = new Person("name", 100, "kg", 2, "m");
		perkgminv = new Person("name", 1, "kg", 2, "m");

		perdkgm = new Person("name", 10000, "dkg", 2, "m");
		perdkgminv = new Person("name", 1, "dkg", 2, "m");

		pergm = new Person("name", 100000, "g", 2, "m");
		pergminv = new Person("name", 1, "g", 2, "m");

		perpm = new Person("name", 220.46, "pound", 2, "m");
		perpminv = new Person("name", 1, "pound", 2, "m");

		perkgcm = new Person("name", 100, "kg", 200, "cm");
		perkgcminv = new Person("name", 1, "kg", 200, "cm");

		perdkgcm = new Person("name", 10000, "dkg", 200, "cm");
		perdkgcminv = new Person("name", 1, "dkg", 200, "cm");

		pergcm = new Person("name", 100000, "g", 200, "cm");
		pergcminv = new Person("name", 1, "g", 200, "cm");

		perpcm = new Person("name", 220.46, "pound", 200, "cm");
		perpcminv = new Person("name", 1, "pound", 200, "cm");

		perkgmm = new Person("name", 100, "kg", 2000, "mm");
		perkgmminv = new Person("name", 1, "kg", 2000, "mm");

		perdkgmm = new Person("name", 10000, "dkg", 2000, "mm");
		perdkgmminv = new Person("name", 1, "dkg", 2000, "mm");

		pergmm = new Person("name", 100000, "g", 2000, "mm");
		pergmminv = new Person("name", 1, "g", 2000, "mm");

		perpmm = new Person("name", 220.46, "pound", 2000, "mm");
		perpmminv = new Person("name", 1, "pound", 2000, "mm");

		perkgi = new Person("name", 100, "kg", 78.74, "inch");
		perkgiinv = new Person("name", 1, "kg", 78.74, "inch");

		perdkgi = new Person("name", 10000, "dkg", 78.74, "inch");
		perdkgiinv = new Person("name", 1, "dkg", 78.74, "inch");

		pergi = new Person("name", 100000, "g", 78.74, "inch");
		pergiinv = new Person("name", 1, "g", 78.74, "inch");

		perpi = new Person("name", 220.46, "pound", 78.74, "inch");
		perpiinv = new Person("name", 1, "pound", 78.74, "inch");
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalid() throws InvalidInput {
		calc.calculateBMI(perinv);
	}

	// FIRST batch of validation tests M

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidKGM() throws InvalidInput {
		calc.calculateBMI(perkgminv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidDKGM() throws InvalidInput {
		calc.calculateBMI(perdkgminv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidGM() throws InvalidInput {
		calc.calculateBMI(pergminv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidPM() throws InvalidInput {
		calc.calculateBMI(perpminv);
	}

	// SECOND batch of validation tests CM

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidKGCM() throws InvalidInput {
		calc.calculateBMI(perkgcminv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidDKGCM() throws InvalidInput {
		calc.calculateBMI(perdkgcminv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidGCM() throws InvalidInput {
		calc.calculateBMI(pergcminv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidPCM() throws InvalidInput {
		calc.calculateBMI(perpcminv);
	}

	// THIRD batch of validation tests MM

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidKGMM() throws InvalidInput {
		calc.calculateBMI(perkgmminv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidDKGMM() throws InvalidInput {
		calc.calculateBMI(perdkgmminv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidGMM() throws InvalidInput {
		calc.calculateBMI(pergmminv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidPMM() throws InvalidInput {
		calc.calculateBMI(perpmminv);
	}

	// FOURTH batch of validation tests INCH

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidKGI() throws InvalidInput {
		calc.calculateBMI(perkgiinv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidDKGI() throws InvalidInput {
		calc.calculateBMI(perdkgiinv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidGI() throws InvalidInput {
		calc.calculateBMI(pergiinv);
	}

	@Test(expected = bmiinterface.InvalidInput.class)
	public void testcalculateBMIInvalidPI() throws InvalidInput {
		calc.calculateBMI(perpiinv);
	}

	// FIRST batch of calculation tests

	@Test
	public void testcalculateBMIKGMVal() {
		try {
			calc.calculateBMI(perkgm);
			assertEquals(perkgm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIDKGMVal() {
		try {
			calc.calculateBMI(perdkgm);
			assertEquals(perdkgm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIGMVal() {
		try {
			calc.calculateBMI(pergm);
			assertEquals(pergm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIPMVal() {
		try {
			calc.calculateBMI(perpm);
			assertEquals(perpm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	// SECOND batch of calculation tests

	@Test
	public void testcalculateBMIKGCMVal() {
		try {
			calc.calculateBMI(perkgcm);
			assertEquals(perkgcm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIDKGCMVal() {
		try {
			calc.calculateBMI(perdkgcm);
			assertEquals(perdkgcm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIGCMVal() {
		try {
			calc.calculateBMI(pergcm);
			assertEquals(pergcm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIPCMVal() {
		try {
			calc.calculateBMI(perpcm);
			assertEquals(perpcm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	// THIRD batch of calculation tests

	@Test
	public void testcalculateBMIKGMMVal() {
		try {
			calc.calculateBMI(perkgmm);
			assertEquals(perkgmm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIDKGMMVal() {
		try {
			calc.calculateBMI(perdkgmm);
			assertEquals(perdkgmm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIGMMVal() {
		try {
			calc.calculateBMI(pergmm);
			assertEquals(pergmm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIPMMVal() {
		try {
			calc.calculateBMI(perpmm);
			assertEquals(perpmm.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	// FORTH batch of calculation tests

	@Test
	public void testcalculateBMIKGIVal() {
		try {
			calc.calculateBMI(perkgi);
			assertEquals(perkgi.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIDKGIVal() {
		try {
			calc.calculateBMI(perdkgi);
			assertEquals(perdkgi.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIGIVal() {
		try {
			calc.calculateBMI(pergi);
			assertEquals(pergi.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

	@Test
	public void testcalculateBMIPIVal() {
		try {
			calc.calculateBMI(perpi);
			assertEquals(perpi.getBMI(), 25, 0.1);
		} catch (InvalidInput ex) {

		}
	}

}
