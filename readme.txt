This is a quick owerview of my project.

For my first step I created a Person class in a different package then the main class, which will hold the Name, metric data and the type of that metric data of a person. 
The initial idea was to separe the types of the metric data by value, but there are some owerlaping brackets, like the CM and Inch intervals. 
That is the reason why I used the Person class as a container that I can pass arround.

As a second step I made an interface in the same package as the Person class. This was done, because the setBMI and setVerdict methods for the Person class are protected, so no1 outside the package can modify it.
The Calculator class implemets the interface. This is where the magic happens. This package also includes a costum exception (InvalidInput). 
First the function checks if the metric types are valid, if not it throws the InvalidInput excention.
After the checks depending on the combination of the data the function selects the correct one, does some casting and calculates the BMI.
When the BMI is ready the verdinct is also set using the brackets found online.

My third step in development was the unit tests. I made a total of 33 testcases, which is abit low, but it would have been abit redundant, so I only tested with the following: 
	-invalid metric type
	-valid data for each data type combination
	-invalid data for each data type combination
	
The last step was to add few demo lines in the main function to demonstrate the outputs.

Made by J�nos Vaszca.